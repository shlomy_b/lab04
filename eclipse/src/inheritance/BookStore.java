// Shlomo Bensimhon
// 1837702

package inheritance;

public class BookStore {
	public static void main(String[] args) {
		
		Book[] b1 = new Book[5];
		
		b1[0] = new Book("A Game Of Thrones", "George R. R. Martin");
		b1[1] = new ElectronicBook("Pet Sematary", "Stephen King", 25);
		b1[2] = new Book("The Innovator", "Walter Isaacson");
		b1[3] = new ElectronicBook("The Hunger Games", "Suzanne Collins", 50);
		b1[4] = new ElectronicBook("The Four", " Scott Galloway", 15);
		
		for(int i = 0; i < b1.length; i++) {
			System.out.print(b1[i].toString());
			System.out.print("\n");
			System.out.print("==================");
			System.out.print("\n");
		}
		
	}
}
