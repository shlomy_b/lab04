// Shlomo Bensimhon
// 1837702

package inheritance;

public class ElectronicBook extends Book{
	
	private int numberBytes;
	
		public ElectronicBook(String title, String author, int numBytes){
			super(title, author);
			this.numberBytes = numBytes;	
		}
		
		public String toString() {
			return ("Title: " + this.title + "\n" + "Author: " + this.getAuthor() + "\n" + "Number of Bytes: " + numberBytes);
		}
}


