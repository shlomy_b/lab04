//Shlomo Bensimhon
//1837702

package geometry;

public interface Shape {
	
	double getArea();
	double getPerimeter(); 
}