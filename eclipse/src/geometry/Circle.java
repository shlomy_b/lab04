//Shlomo Bensimhon
//1837702

package geometry;

public class Circle implements Shape{
	
	private double radius;
	
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return ((this.radius * this.radius) * Math.PI);
	}
	
	public double getPerimeter() {
		return ((2 * Math.PI) * this.radius);
	}
}
