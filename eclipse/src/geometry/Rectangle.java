package geometry;

public class Rectangle implements Shape{
	private double length;
	private double width;
	
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
		
	}
	
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getArea() {
		return (this.width * this.length);
	}
	
	public double getPerimeter() {
		return ((this.width + this.length) * 2);
	}
}





