package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
	
		Shape[] s1 = new Shape[5];
		
		s1[0] = new Rectangle(5.0, 10.0);
		s1[1] = new Rectangle(2.0, 4.0);
		s1[2] = new Circle(8.0);
		s1[3] = new Circle(16.0);
		s1[4] = new Square(10.0);
		
		for(int i = 0; i < s1.length; i++) {
			System.out.println("Area: " + s1[i].getArea());
			System.out.println("Perimeter: " + s1[i].getPerimeter());
			System.out.println("===================================");
		}
	}
}
